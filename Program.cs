﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Bezao_Regex_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            string textEmails = "frank@i34, pope@gmail.com,pope.com, summy@gmail@.com, pope@yahoomail.com";
            GetEmails(textEmails);
            Console.ReadLine();
        }

        static void GetEmails(string email)
        {
            Regex verifyEmail = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",RegexOptions.IgnoreCase);
            MatchCollection verifiedEmail = verifyEmail.Matches(email);
            StringBuilder stringBuilder = new StringBuilder();

            foreach (Match match in verifiedEmail)
            {
                stringBuilder.Append($"{match.Value} ");
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
